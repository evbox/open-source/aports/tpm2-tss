#
# Copyright (C) EVBox Intelligence B.V.
#

stages:
  - package
  - publish


# Common parameters
# =============================================================================
default:
  tags:
    - docker

.artifact_paths: &artifact_paths
  paths:
    - "output/**/*.apk"

.git_login: &git_login
  - git config --global user.name "${BOT_USER_NAME}"
  - git config --global user.email "${BOT_USER_MAIL}"
  - git config --global credential.helper "cache --timeout=2147483647"
  - |
    _git_credentials="url=${CI_SERVER_URL}"
    _git_credentials="${_git_credentials}\nusername=${BOT_USER_NAME}"
    _git_credentials="${_git_credentials}\npassword=${CI_PERSONAL_TOKEN}\n\n"
    printf "${_git_credentials}" | git credential approve

.git_logout: &git_logout
  - git credential-cache exit


# Git authentication
# ===========================================================================
.git:
  image: "registry.hub.docker.com/gitscm/git:latest"
  before_script:
    - *git_login
  after_script:
    - *git_logout
  variables:
    GIT_DEPTH: "0"


# Build alpine package
# =============================================================================
.packaging:
  image: "registry.gitlab.com/esbs/package_builder-alpine/${TARGET_ARCH}/package_builder-alpine:latest"
  stage: package
  artifacts:
    <<: *artifact_paths
    expire_in: "1 month"
  before_script:
    - cp "${CI_SIGNING_KEY}" "/run/${CI_SIGNING_KEY_NAME}"
    - cp "${CI_SIGNING_KEY_PUB}" "/run/${CI_SIGNING_KEY_NAME}.pub"
  script:
    - package_builder-alpine.sh -k "/run/${CI_SIGNING_KEY_NAME}"
  after_script:
    - rm -rf "/run/${CI_SIGNING_KEY_NAME}"
    - rm -rf "/run/${CI_SIGNING_KEY_NAME}.pub"

package_arm32v7:
  extends: .packaging
  variables:
    TARGET_ARCH: "arm32v7"
  tags:
    - qemu

package_amd64:
  extends: .packaging
  variables:
    TARGET_ARCH: "amd64"


# Publish a packages to package registry
# =============================================================================
.publish:
  image: curlimages/curl:latest
  stage: publish
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+(\.\d+){2,}(-rc\d+)?$/'
  script:
    - _ci_commit_tag="${CI_COMMIT_TAG#v}"
    - _apk_dir_name="${_ci_commit_tag//[-]/_}"
    - |
      ls -l -R
      cd output/aports/${TARGET_ARCH}/
      for _apk_file in ./*
      do
        echo "Publish a ${_apk_file} to package registry"
        curl \
              --fail \
              --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
              --output "/dev/null" \
              --request "PUT" \
              --retry 3 \
              --show-error \
              --silent \
              --upload-file "${_apk_file}" \
              --write-out "Uploaded file: '${_apk_file}'; HTTP response: %{http_code}\n\n" \
              "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${TARGET_ARCH}/${_apk_dir_name}/${_apk_file}"
      done

publish_arm32v7:
  extends: .publish
  dependencies:
    - package_arm32v7
  variables:
    TARGET_ARCH: "armv7"
  tags:
    - qemu

publish_amd64:
  extends: .publish
  dependencies:
    - package_amd64
  variables:
    TARGET_ARCH: "x86_64"
